% Title
% Organization\footnote{This is a footnote}
% Date

<!-- 
This is a comment in R markdown
-->


<!--
to create output pdf, run this in terminal:
  cd '/Users/ericpgreen/Dropbox/bitbucket/knitr-pandoc/scripts'; pandoc -V geometry:margin=1in -o myreport.pdf analysis.md --csl=apa.csl -N --template=latex.template
-->




Heading 1
============

Text

Heading 2
--------

Text

### Heading 3

Text




Refer to Table \ref{tbl:mytable} in text by referencing label.

<!-- table -->
\begin{table}[H]
  \begin{threeparttable}
  \caption{My title}
  \label{tbl:mytable}
  \begin{tabular}{lrrrr}
  \toprule
  Item &  M & SD & Min  &  Max    \\
  \midrule
  \input{desc-var12}
  \bottomrule
  \end{tabular}
  \begin{tablenotes}
  \small
  \item A note goes here.
  \end{tablenotes}
  \end{threeparttable}
\end{table}

Here is an inline code reference. The mean of var1 is 52.15.
